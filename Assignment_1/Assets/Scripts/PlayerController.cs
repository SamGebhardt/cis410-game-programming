using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
      
    public float speed = 20.0f;

    private Rigidbody rb;
    private int count;

    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private float movementX;
    private float movementY;

    private bool double_jump;
  

    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        double_jump = true;
        rb = GetComponent<Rigidbody>();
        winTextObject.SetActive(false);

    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;


    }

    private void jump() {

        Vector3 movement = new Vector3(0.0f, 1.0f, 0.0f);
        rb.AddForce(movement * speed, ForceMode.Impulse);
    }

    private void Update() {

        // y = 0.5 if on ground
        Vector3 current_pos = rb.position;

        if (current_pos[1] == 0.5f) {
            double_jump = false;
        }

        if (Keyboard.current.spaceKey.wasPressedThisFrame) {

            // On the ground and can single jump
            if (current_pos[1] == 0.5f) {
                jump();
                //Debug.Log("Single");

                double_jump = true;
                return;
            }

            // In the air and can jump again
            else if (double_jump && current_pos[1] > 0.5f) {
                jump();
                //Debug.Log("Double");
                double_jump = false;
            }
        }

        Vector3 movement = new Vector3(movementX * 10, 0.0f, movementY * 10);
        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("pickme"))
        {
            other.gameObject.SetActive(false);
            count++;

            SetCountText();

        }
    }

    void SetCountText() {
        countText.text = "Count: " + count.ToString();

        if (count >= 10)
        {
            // Set the text value of your 'winText'
            winTextObject.SetActive(true);
        }
    }

}